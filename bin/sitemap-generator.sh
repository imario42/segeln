#!/bin/sh

cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
EOF

for i in `find . -type f \( \( -name '*.html' -o -name '*.pdf' \) -a ! -name 'google*.html' \) -print`
do
  FILETIME=`date -r "$i" -Iseconds`
  FNAME=$i
  cat <<EOF
    <url>
      <loc>https://segeln.datenwort.at/$FNAME</loc>
      <lastmod>$FILETIME</lastmod>
      <changefreq>monthly</changefreq>
    </url>
EOF
done

cat <<EOF
</urlset>
EOF
